//package kelompokb4.moviedb.Search.controller;
//
//import kelompokb4.moviedb.movie.core.Movie;
//import kelompokb4.moviedb.movie.service.MovieService;
//import kelompokb4.moviedb.search.controller.SearchController;
//import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//import org.springframework.web.context.WebApplicationContext;
//
//import static org.mockito.Mockito.lenient;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
//
//@WebMvcTest(controllers = SearchController.class)
//public class SearchControllerTest {
//    @Autowired
//    private MockMvc mockMvc;
//
//    @MockBean
//    private MovieService movieService;
//
//    private Movie movie;
//
//    @Test
//    public void canAccessResultbyTitle() throws Exception {
//        mockMvc.perform(get("/result?keyword=mo&filter=Title"))
//                .andExpect(status().isOk());
//    }
//
//    @Test
//    public void canAccessResultbyYear() throws Exception {
//        mockMvc.perform(get("/result?keyword=mo&filter=Year"))
//                .andExpect(status().isOk());
//    }
//
//    @Test
//    public void canAccessResultbyRating() throws Exception {
//        mockMvc.perform(get("/result?keyword=mo&filter=Rating"))
//                .andExpect(status().isOk());
//    }
//
//}
