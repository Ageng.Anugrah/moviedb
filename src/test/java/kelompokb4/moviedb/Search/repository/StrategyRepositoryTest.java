//package kelompokb4.moviedb.Search.repository;
//
//import kelompokb4.moviedb.movie.core.Movie;
//import kelompokb4.moviedb.review.core.Review;
//import kelompokb4.moviedb.search.core.SearchByRating;
//import kelompokb4.moviedb.search.core.SearchByTitle;
//import kelompokb4.moviedb.search.core.SearchByYear;
//import kelompokb4.moviedb.search.repository.StrategyRepository;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.mockito.Mock;
//import org.mockito.junit.jupiter.MockitoExtension;
//import org.springframework.web.client.RestTemplate;
//
//import java.util.*;
//
//import static org.junit.jupiter.api.Assertions.assertNotNull;
//import static org.mockito.Mockito.lenient;
//
//@ExtendWith(MockitoExtension.class)
//public class StrategyRepositoryTest {
//    @Mock
//    private StrategyRepository strategyRepository;
//
//    @Mock
//    private RestTemplate restTemplate;
//
//    Movie movie;
//
//    private Map<String, Comparator<Movie>> searchBy = new HashMap<>();
//
//    @BeforeEach
//    void init(){
//        searchBy.put("Title", new SearchByTitle());
//        searchBy.put("Year", new SearchByYear());
//        searchBy.put("Rating", new SearchByRating());
//
//    }
//
//    @Test
//    public void getSearchByTypeTitleTest() {
//        Comparator<Movie> searchByType = strategyRepository.getSearchByType("Title");
//        assertNotNull(searchByType);
//    }
//
//    @Test
//    public void getSearchByTypeYearTest() {
//        Comparator<Movie> searchByType = strategyRepository.getSearchByType("Year");
//        assertNotNull(searchByType);
//    }
//
//    @Test
//    public void getSearchByTypeRatingTest() {
//        Comparator<Movie> searchByType = strategyRepository.getSearchByType("Rating");
//        assertNotNull(searchByType);
//    }
//
//}