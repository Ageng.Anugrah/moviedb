package kelompokb4.moviedb.profile.service;

import kelompokb4.moviedb.auth.model.Role;
import kelompokb4.moviedb.auth.model.User;
import kelompokb4.moviedb.auth.repository.UserRepository;
import kelompokb4.moviedb.movie.core.Movie;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
public class ProfileServiceTest {

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private ProfileServiceImpl profileService;

    Movie movie;

    private User user;
    private Role role;

    @BeforeEach
    void init(){
        user = new User("sofita","sofita bin peesde","sofita@gmail.com","iniPassword");
        role = new Role("USER");
        user.setRoles(role);

        lenient().when(userRepository.findByUsername("sofita")).thenReturn(user);
    }

    @Test
    public void testGetUserByUsername() {
        User getUser = profileService.findUserByUsername("sofita");
        assertNotNull(getUser);
    }
}
