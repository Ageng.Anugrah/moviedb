package kelompokb4.moviedb.profile.controller;

import kelompokb4.moviedb.auth.model.Role;
import kelompokb4.moviedb.auth.model.User;
import kelompokb4.moviedb.auth.service.MyUserDetailService;
import kelompokb4.moviedb.profile.service.ProfileService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.lenient;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = ProfileController.class)
public class ProfileControllerTest {

    @Autowired
    MockMvc mockMvc;


    @MockBean
    private ProfileService profileService;

    @MockBean
    MyUserDetailService myUserDetailService;


    private User user;
    private Role role;

    @BeforeEach
    public void init() {
        user = new User("sofita","sofita bin peesde","sofita@gmail.com","iniPassword");
        role = new Role();
        role.setRole("USER");
        user.setRoles(role);

        lenient().when(profileService.findUserByUsername("sofita")).thenReturn(user);
    }

    @Test
    public void testHomeUrl() throws Exception{
        mockMvc.perform(get("/profile/sofita")).andExpect(status().isOk());
    }

}
