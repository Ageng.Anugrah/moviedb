package kelompokb4.moviedb.review.core;

import kelompokb4.moviedb.movie.core.Movie;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class ReviewTest {
    private Class<?> reviewClass;

    @BeforeEach
    public void setUp() throws Exception {
        reviewClass = Class.forName("kelompokb4.moviedb.review.core.Review");
    }

    @Test
    public void testReviewNilai() throws Exception {
        Movie movie = new Movie();
        movie.setIdMovie(1);
        movie.setTitle("title");
        movie.setYear("1998");
        movie.setDescription("desc");
        Review review = new Review(10, "Mantap nih Bos", movie.getIdMovie(), "McDumbDumb");
        assertEquals(10, review.getNilai());
        assertEquals("Mantap nih Bos", review.getKomen());
        assertEquals(movie.getIdMovie(), review.getIdMovie());
        assertEquals("McDumbDumb", review.getUsername());
        assertEquals(review.getIdReview(), review.getIdReview());
        assertEquals(review.getTanggalPost(), review.getTanggalPost());
    }

}
