package kelompokb4.moviedb.review.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import kelompokb4.moviedb.auth.service.MyUserDetailService;
import kelompokb4.moviedb.auth.service.UserService;
import kelompokb4.moviedb.movie.core.Movie;
import kelompokb4.moviedb.movie.service.MovieService;
import kelompokb4.moviedb.review.core.ReviewDtoBase;
import kelompokb4.moviedb.review.service.ReviewService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@WebMvcTest(controllers = ReviewController.class)
public class ReviewControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ReviewService reviewService;

    @MockBean
    private MovieService movieService;

    @MockBean
    private MyUserDetailService myUserDetailService;

    @MockBean
    private UserService userService;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private Movie movie;
    private ReviewDtoBase rev;

    @BeforeEach
    public void setUp(){
        movie = new Movie();
        movie.setIdMovie(1);
        movie.setTitle("title");
        movie.setYear("1998");
        movie.setDescription("desc");
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        rev = new ReviewDtoBase();
        rev.setIdmovie(String.valueOf(movie.getIdMovie()));
        rev.setKomen("Komen");
        rev.setNilai("10");
    }


    @Test
    public void canAccesReviewPage() throws Exception{
        mockMvc.perform(get("/reviews"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("getReviews"))
                .andExpect(view().name("review/review"));
    }

    @Test
    public void reviewPageHandlerIsGetReviews() throws Exception{
        mockMvc.perform(get("/reviews"))
                .andExpect(handler().methodName("getReviews"));
    }

    @Test
    public void reviewPageViewNameIsReview() throws Exception{
        mockMvc.perform(get("/reviews"))
                .andExpect(view().name("review/review"));
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    @WithMockUser(username = "admin", authorities = {"ADMIN", "USER"})
    public void canCreateReview() throws Exception{
        when(movieService.getMovieByIdMovie(movie.getIdMovie())).thenReturn(movie);

        mockMvc.perform(
                post("/createreview")
                .contentType(MediaType.APPLICATION_JSON).content(mapToJson(rev)))
                .andExpect(status().isOk());

    }

}
