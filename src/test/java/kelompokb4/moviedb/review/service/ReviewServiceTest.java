package kelompokb4.moviedb.review.service;

import kelompokb4.moviedb.movie.core.Movie;
import kelompokb4.moviedb.movie.service.MovieService;
import kelompokb4.moviedb.review.core.Review;
import kelompokb4.moviedb.review.core.ReviewDtoBase;
import kelompokb4.moviedb.review.repository.ReviewRepository;
import kelompokb4.moviedb.review.service.error.AlreadyReviewException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ReviewServiceTest {
    @Mock
    private ReviewRepository reviewRepository;


    @Mock
    private MovieService movieService;



    @InjectMocks
    private ReviewServiceImpl reviewService;

    @Mock
    private Authentication auth;

    private Movie movie;
    private Review review;
    private ReviewDtoBase rev;
    private List<Review> reviews;

    @BeforeEach
    public void setUp() {

        movie = new Movie();
        movie.setIdMovie(1);
        movie.setTitle("title");
        movie.setYear("1998");
        movie.setDescription("desc");

        review = new Review(10, "Komen", movie.getIdMovie(), "Ini nama");
        reviews = new ArrayList<>();
        reviews.add(review);

        rev = new ReviewDtoBase();
        rev.setIdmovie(String.valueOf(1));
        rev.setKomen("Komen");
        rev.setNilai("10");

        SecurityContextHolder.getContext().setAuthentication(auth);
    }

    @Test
    @WithMockUser(username = "admin", authorities = {"ADMIN", "USER"})
    public void testServiceCanCreateReview() throws AlreadyReviewException{
        lenient().when(auth.getName()).thenReturn("Ini nama");
        lenient().when(reviewRepository.findByIdMovie(1)).thenReturn(reviews);
        lenient().when( movieService.getMovieByIdMovie(1)).thenReturn(movie);
        try{
            reviewService.createReview(rev);
        }catch (AlreadyReviewException e){
            e.getMessage();
        }
        reviews.clear();
        lenient().when(reviewRepository.findByIdMovie(1)).thenReturn(reviews);
        try{
            reviewService.createReview(rev);
        }catch (AlreadyReviewException e){
            e.getMessage();
        }
    }

    @Test
    public void testGetListReview() {
        when(reviewRepository.findAll()).thenReturn(reviews).thenReturn(reviews);
        assertEquals(reviews, reviewService.getListReview());
    }

    @Test
    public void testServiceCanGetReviewByMovie() {
        lenient().when(reviewRepository.findByIdMovie(1)).thenReturn(reviews);
        assertNotNull(reviewService.getReviewByMovie(movie));
    }
}



