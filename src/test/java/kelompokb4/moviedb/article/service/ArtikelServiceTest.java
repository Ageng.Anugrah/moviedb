package kelompokb4.moviedb.article.service;

import kelompokb4.moviedb.article.core.artikel.Artikel;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith(MockitoExtension.class)
public class ArtikelServiceTest {

    @InjectMocks
    private ArtikelServiceImpl artikelService;

    private Artikel artikel;

    @BeforeEach
    public void setUp() {
        artikel = new Artikel("judul", "isi", "McDumbDumb");
    }

    @Test
    public void testServiceCanCreateArtikel() {
        artikelService.createArtikel(artikel);
        assertNotNull(artikelService.getListArtikel());
    }

    @Test
    public void testServiceCanGetArtikel() {
        List<Artikel> artikels = new ArrayList<>();
        artikels.add(artikel);
        Artikel result = artikelService.getArtikelById(1);
        assertNotNull(result);
    }

}
