package kelompokb4.moviedb.article.controller;

import kelompokb4.moviedb.article.core.artikel.Artikel;
import kelompokb4.moviedb.article.service.ArtikelService;
import kelompokb4.moviedb.auth.service.MyUserDetailService;
import kelompokb4.moviedb.auth.service.UserService;
import kelompokb4.moviedb.movie.service.MovieService;
import kelompokb4.moviedb.review.service.ReviewService;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@WebMvcTest(controllers = ArtikelController.class)
public class ArtikelControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ReviewService reviewService;

    @MockBean
    private MovieService movieService;

    @MockBean
    private ArtikelService artikelService;

    @MockBean
    private MyUserDetailService myUserDetailService;

    @MockBean
    private UserService userService;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private Artikel artikel;

    @BeforeEach
    public void setUp() {
        artikel = new Artikel("Judul", "Isi", "McDumbDumb");
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void canAccessArticlePage() throws Exception {
        mockMvc.perform(get("/artikels"))
                .andExpect(status().isOk());
    }

    @Test
    public void articlePageMethodHandlerIsgetArtikels() throws Exception {
        mockMvc.perform(get("/artikels"))
                .andExpect(handler().methodName("getArtikels"));
    }

    @Test
    public void articlePageViewNameIsArtikel() throws Exception {
        mockMvc.perform(get("/artikels"))
                .andExpect(view().name("article/artikel"));
    }

    @Test
    @WithMockUser(username = "admin", authorities = {"ADMIN", "USER"})
    public void canAccessCreateArticlePage() throws Exception {
        mockMvc.perform(get("/createartikel"))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(username = "admin", authorities = {"ADMIN", "USER"})
    public void canCreateArtikel() throws Exception {
        mockMvc.perform(post("/createartikel")
                .param("judul", "judul")
                .param("isi", "isi")).andExpect(status().is3xxRedirection());
    }

    @Test
    @WithMockUser(username = "anonymousUser")
    public void cannotCreateArtikel() throws Exception {
        mockMvc.perform(post("/createartikel")
                .param("judul", "judul")
                .param("isi", "isi")).andExpect(status().is3xxRedirection());
    }

    @Test
    public void canAccessArticle() throws Exception {
        artikelService.createArtikel(artikel);
        when(artikelService.getArtikelById(artikel.getIdArtikel())).thenReturn(artikel);
        mockMvc.perform(get("/artikel/" + artikel.getIdArtikel()))
                .andExpect(status().isOk());
    }


}
