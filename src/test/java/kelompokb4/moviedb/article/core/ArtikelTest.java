package kelompokb4.moviedb.article.core;

import kelompokb4.moviedb.article.core.artikel.Artikel;
import kelompokb4.moviedb.movie.core.Movie;
import kelompokb4.moviedb.review.core.Review;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class ArtikelTest {
    private Class<?> artikelClass;

    @BeforeEach
    public void setUp() throws Exception {
        artikelClass = Class.forName("kelompokb4.moviedb.article.core.artikel.Artikel");
    }

    @Test
    public void testReviewNilai() throws Exception {
        Artikel artikel = new Artikel("Judul", "Isi", "McDumbDumb");
        assertEquals("Judul", artikel.getJudul());
        assertEquals("Isi", artikel.getIsi());
    }

}
