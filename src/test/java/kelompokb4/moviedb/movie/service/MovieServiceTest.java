package kelompokb4.moviedb.movie.service;

import kelompokb4.moviedb.movie.core.Movie;
import kelompokb4.moviedb.review.core.Review;
import kelompokb4.moviedb.review.repository.ReviewRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class MovieServiceTest {


    @Mock
    private ReviewRepository reviewRepository;

    @Mock
    private RestTemplate restTemplate;

    @InjectMocks
    private MovieServiceImpl movieServiceImpl;

    Movie movie;
    ArrayList<Review> reviews;

    @BeforeEach
    void init(){
        movie = new Movie();
        movie.setTitle("Mortal Kombat");
        movie.setIdMovie(1);
        movie.setUrl("https://image.tmdb.org/t/p/w400//nkayOAUBUu4mMvyNf9iHSUiPjF1.jpg");
        movie.setDescription("mortalkombat yeu");
        movie.setRating(0);
        ArrayList<Movie> movies = new ArrayList<>();
        movies.add(movie);

        Review review = new Review(10, "Komen", movie.getIdMovie(), "Ini nama");
        reviews = new ArrayList<>();
        reviews.add(review);

        lenient().when(restTemplate.getForObject("https://moviedb-movie.herokuapp.com/movie/1", Movie.class)).thenReturn(movie);
        lenient().when(reviewRepository.findByIdMovie(1)).thenReturn(reviews);
    }

    @Test
    public void getMovieByIdTest() {
        Movie getMovie = movieServiceImpl.getMovieByIdMovie(337404);
        assertNotNull(getMovie);
    }

    @Test
    public void getListMovieTest() {
        List<Movie> getMovies = movieServiceImpl.getListMovie();
        assertNotNull(getMovies);
    }

    @Test
    public void updateRatingTest(){
        try {
            movieServiceImpl.updateRating(movie);
            assertEquals(0.0, movie.getRating());
        }catch (HttpServerErrorException e){
            e.getMessage();
        }



    }

}