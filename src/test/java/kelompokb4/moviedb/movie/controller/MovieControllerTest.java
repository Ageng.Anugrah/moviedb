package kelompokb4.moviedb.movie.controller;

import kelompokb4.moviedb.auth.service.MyUserDetailService;
import kelompokb4.moviedb.comment.service.ChildCommentService;
import kelompokb4.moviedb.comment.service.ParentCommentService;
import kelompokb4.moviedb.movie.core.Movie;
import kelompokb4.moviedb.movie.service.MovieService;
import kelompokb4.moviedb.review.service.ReviewService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.WebApplicationContext;

import static org.mockito.Mockito.lenient;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest(controllers = MovieController.class)
class MovieControllerTest {
    @Autowired
    MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @MockBean
    private MovieService movieService;

    @MockBean
    private ReviewService reviewService;

    @MockBean
    private MyUserDetailService myUserDetailService;

    @MockBean
    private ParentCommentService parentCommentService;

    @MockBean
    private ChildCommentService childCommentService;


    private Movie movie;

    @BeforeEach
    public void init() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        movie = new Movie();
        movie.setTitle("Mortal Kombat");
        movie.setIdMovie(1);
        movie.setUrl("https://image.tmdb.org/t/p/w400//nkayOAUBUu4mMvyNf9iHSUiPjF1.jpg");
        movie.setDescription("mortalkombat yeu");

        lenient().when(movieService.getMovieByIdMovie(1)).thenReturn(movie);
    }

    @Test
    void testHomeUrl() throws Exception{
        mockMvc.perform(get("/")).andExpect(status().isOk());
    }

    @Test
    void getMovieByIdTest() throws Exception{
        mockMvc.perform(get("/movie/1").requestAttr("movie",movie)).andExpect(status().isOk());
    }
}
