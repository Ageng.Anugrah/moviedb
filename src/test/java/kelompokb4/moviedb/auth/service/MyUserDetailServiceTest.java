package kelompokb4.moviedb.auth.service;


import kelompokb4.moviedb.auth.model.Role;
import kelompokb4.moviedb.auth.model.User;
import kelompokb4.moviedb.auth.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.userdetails.UserDetails;

import static org.mockito.Mockito.lenient;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class MyUserDetailServiceTest {

    @Mock
    private UserRepository userRepository;

    @Mock
    private UserServiceImpl userServiceImpl;

    @InjectMocks
    private MyUserDetailService myUserDetailService;

    private User user;
    private Role role;

    @BeforeEach
    public void setUp(){
        user = new User("sofita","sofita bin peesde","sofita@gmail.com","iniPassword");
        role = new Role("USER");
        user.setRoles(role);
        userRepository.save(user);
    }

    @Test
    void testCustomDetailService(){
        lenient().when(userServiceImpl.findUserByUsername("sofita")).thenReturn(user);
        UserDetails myUserDetail = myUserDetailService.loadUserByUsername("sofita");

        assertNotNull(myUserDetail);
    }
}
