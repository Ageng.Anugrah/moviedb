package kelompokb4.moviedb.auth.service;


import kelompokb4.moviedb.auth.model.Role;
import kelompokb4.moviedb.auth.model.User;
import kelompokb4.moviedb.auth.repository.RoleRepository;
import kelompokb4.moviedb.auth.repository.UserRepository;
import kelompokb4.moviedb.auth.service.UserService;
import kelompokb4.moviedb.auth.service.UserServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.lenient;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class UserServiceImplTest {

    @Mock
    private UserRepository userRepository;
    @Mock
    private RoleRepository roleRepository;
    @InjectMocks
    private UserServiceImpl userServiceImpl;

    @Test
    void testUserServiceImpl(){
        User user = new User("sofita","sofita bin peesde","sofita@gmail.com","iniPassword");
        lenient().when(userRepository.findByUsername("sofita")).thenReturn(user);
        lenient().when(userRepository.findByEmail("sofita@gmail.com")).thenReturn(user);
        lenient().when(userRepository.findByName("sofita bin peesde")).thenReturn(user);
        userRepository.save(user);
        userServiceImpl.registerUser("andi", "andi bin budi", "andi@gmail.com", "iniPassword");

        assertEquals("sofita@gmail.com", user.getEmail());
        assertNotNull(userServiceImpl.findUserByUsername("sofita"));
        assertNotNull(userServiceImpl.findUserByEmail("sofita@gmail.com"));
        assertNotNull(userServiceImpl.findUserByName("sofita bin peesde"));

    }




}
