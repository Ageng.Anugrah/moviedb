package kelompokb4.moviedb.auth.controller;

import kelompokb4.moviedb.auth.service.MyUserDetailService;
import kelompokb4.moviedb.auth.service.UserService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = AuthController.class)
public class AuthControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    MyUserDetailService myUserDetailService;

    @MockBean
    UserService userService;

    @Test
    void testAuthController() throws Exception{
        mvc.perform(get("/login")).andExpect(status().isOk());
        String name = "orang";
        String username = "org";
        String email = "email@gmail.com";
        String password = "123";
        mvc.perform(post("/createUser")
                    .param("name",name)
                    .param("username",username)
                    .param("email",email)
                    .param("password",password)
                    .param("passwordConfirm", password)
                    .with(csrf()))
                .andExpect(redirectedUrl("/"));
    }
}
