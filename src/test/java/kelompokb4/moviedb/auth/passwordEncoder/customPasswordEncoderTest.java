package kelompokb4.moviedb.auth.passwordEncoder;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;


import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
class customPasswordEncoderTest {

    CustomPasswordEncoder customPasswordEncoder;

    @BeforeEach
    public void setUp(){
        customPasswordEncoder = CustomPasswordEncoder.getInstance();
    }

    @Test
    void testCustomPasswordEncoder(){
        String rawPassword = "iniPassword";
        String encryptedPassword = customPasswordEncoder.encode(rawPassword);
        Boolean match = customPasswordEncoder.matches(rawPassword,encryptedPassword);


        assertTrue(match);

    }
}
