$(document).ready(function () {
    $("#reviewform").submit(function(){
        event.preventDefault();
        console.log("Submitted");
        let reviewDto ={};
        reviewDto['nilai'] = $("#nilai").val();
        reviewDto['komen'] = $("#komen").val();
        reviewDto['idmovie'] = $("#idmovie").val();
        console.log(reviewDto);
        $.ajax({
            type : "POST",
            headers: {"X-CSRF-TOKEN": $("input[name='_csrf']").val()},
            contentType : "application/json",
            url : "/createreview",
            data : JSON.stringify(reviewDto),
            dataType : 'json',
            success : function (response) {
                console.log(response);
                let rating;
                $.ajax({
                    type:"GET",
                    url:"https://moviedb-movie.herokuapp.com/movie/" + reviewDto['idmovie'],
                    dataType:'json',
                    success : function (response){
                        rating = response.rating;
                    }
                })
                var rev = response
                let komen = rev.komen;
                let tanggalPost = rev.tanggalPost;
                let nilai = rev.nilai;
                let nama = rev.username;

                $("#kumpulanreview").append(`
                 <div>
                                <div class="my-4">
                                    <div class="panel w-50">
                                        <div class="panel-body">
                                            <div class="media-left">
                                                <a href="#">
                                                    <img src="/images/user.jpg" title="One movies" alt=" "/>
                                                </a>
                                            </div>
                                            <div class="media-body">
                                                <h5 class="panel-title" style="color: black">` + nama + `</h5>
                                                <a href="#"><i class="fa fa-star" aria-hidden="true">` + nilai + `</i></a>
                                                <p class="panel-text" style="color: black">`+komen+`</p>
                                                <br>
                                                <p style="text-align:right;">`+tanggalPost+`</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                `);
                $("#ratingmovie").text(rating);
                $('#reviewform').each(function(){
                    this.reset();
                });
            },
            error : function (e) {
                alert("Anda sudah pernah menambahkan review untuk film ini");
            }
        })
    });
});