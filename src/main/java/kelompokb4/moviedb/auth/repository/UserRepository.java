package kelompokb4.moviedb.auth.repository;

import kelompokb4.moviedb.auth.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, String> {
    User findByEmail(String email);
    User findByName(String name);
    User findByUsername(String Username);
}
