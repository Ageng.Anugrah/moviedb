package kelompokb4.moviedb.auth.model;

import lombok.Builder;
import lombok.Data;

import javax.persistence.*;

@Data
@Builder
@Entity
@Table(name = "roles")
public class Role {

    @Id
    @Column(name = "role", nullable = false)
    private String roleName;

    public void setRole(String role) {
        this.roleName = role;
    }

    public Role(){
        super();
    }
    public Role(String role){
        this.roleName = role;
    }

}
