package kelompokb4.moviedb.auth.service;


import kelompokb4.moviedb.auth.model.Role;
import kelompokb4.moviedb.auth.model.User;
import kelompokb4.moviedb.auth.passwordEncoder.CustomPasswordEncoder;
import kelompokb4.moviedb.auth.repository.RoleRepository;
import kelompokb4.moviedb.auth.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService{

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;


    public User findUserByEmail(String email){
        return userRepository.findByEmail(email);
    }

    public User findUserByName(String name){
        return userRepository.findByName(name);
    }

    public User findUserByUsername(String username){ return userRepository.findByUsername(username);}

    public User registerUser(String username, String name,String email, String rawPassword){

        PasswordEncoder encoder = CustomPasswordEncoder.getInstance();
        String encryptedPassword = encoder.encode(rawPassword);
        Role role = roleRepository.findByRoleName("USER");

        User newUser = new User(username,name, email, encryptedPassword);
        newUser.setRoles(role);

        userRepository.save(newUser);
        roleRepository.save(role);

        return newUser;
    }


}
