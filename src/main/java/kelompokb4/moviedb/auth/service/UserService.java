package kelompokb4.moviedb.auth.service;

import kelompokb4.moviedb.auth.model.User;

public interface UserService {

    public User findUserByUsername(String username);
    public User findUserByEmail(String email);
    public User findUserByName(String name);
    public User registerUser(String username, String name,String email, String password);




}
