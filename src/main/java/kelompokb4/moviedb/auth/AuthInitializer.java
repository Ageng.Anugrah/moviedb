package kelompokb4.moviedb.auth;


import kelompokb4.moviedb.auth.model.Role;
import kelompokb4.moviedb.auth.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class AuthInitializer {

    @Autowired
    private RoleRepository roleRepository;

    @PostConstruct
    public void init(){
        roleRepository.save(new Role("USER"));
        roleRepository.save(new Role("SUPERUSER"));

    }
}
