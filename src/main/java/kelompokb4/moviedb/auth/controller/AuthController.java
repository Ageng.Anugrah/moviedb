package kelompokb4.moviedb.auth.controller;

import kelompokb4.moviedb.auth.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller

public class AuthController {

    @Autowired
    private UserService userService;

    @PostMapping("/createUser")
    public String createUser(
            @RequestParam(value = "username") String username,
            @RequestParam(value = "name") String name,
            @RequestParam(value = "email") String email,
            @RequestParam(value = "password") String password,
            @RequestParam(value = "passwordConfirm") String passConfirm
    ){
        if(!password.equals(passConfirm)){
            return "redirect:/register";
        }
        userService.registerUser(username, name, email, password);
        return "redirect:/";
    }
}
