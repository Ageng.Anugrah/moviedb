package kelompokb4.moviedb.auth.passwordEncoder;

import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.password.PasswordEncoder;

public class CustomPasswordEncoder implements PasswordEncoder {

    private static CustomPasswordEncoder customPasswordEncoder = new CustomPasswordEncoder();

    public static CustomPasswordEncoder getInstance(){
        return customPasswordEncoder;
    }
    @Override
    public String encode(CharSequence rawPassword){
        return BCrypt.hashpw(rawPassword.toString(), BCrypt.gensalt(12));
    }

    @Override
    public boolean matches(CharSequence rawPassword, String encodedPassword) {
        return BCrypt.checkpw(rawPassword.toString(), encodedPassword);
    }
}
