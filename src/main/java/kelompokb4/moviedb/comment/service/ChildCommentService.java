package kelompokb4.moviedb.comment.service;

import kelompokb4.moviedb.comment.core.ParentComment;
import kelompokb4.moviedb.comment.core.ChildComment;
import kelompokb4.moviedb.comment.core.ChildCommentDtoBase;

import java.util.List;


public interface ChildCommentService {
    ChildComment createChildComment(ChildCommentDtoBase childComment);
    Iterable<ChildComment> getListChildComment();
    List<ChildComment> getChildCommentByParentComment(ParentComment parentComment);

}
