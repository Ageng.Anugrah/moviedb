package kelompokb4.moviedb.comment.service;

import kelompokb4.moviedb.movie.core.Movie;
import kelompokb4.moviedb.comment.core.ParentComment;

import java.util.List;


public interface ParentCommentService {
    ParentComment createParentComment(ParentComment parentComment);
    ParentComment getParentCommentByIdParentComment(int idParentComment);
    Iterable<ParentComment> getListParentComment();
    List<ParentComment> getParentCommentByMovie(Movie movie);
    
}
