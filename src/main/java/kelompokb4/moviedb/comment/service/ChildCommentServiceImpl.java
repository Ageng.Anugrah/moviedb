package kelompokb4.moviedb.comment.service;

import kelompokb4.moviedb.comment.core.ChildComment;
import kelompokb4.moviedb.comment.core.ChildCommentDto;
import kelompokb4.moviedb.comment.core.ParentComment;
import kelompokb4.moviedb.comment.repository.ChildCommentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kelompokb4.moviedb.comment.service.ParentCommentService;
import kelompokb4.moviedb.comment.core.ChildCommentDtoBase;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;

@Service
public class ChildCommentServiceImpl implements ChildCommentService{
    @Autowired
    private ChildCommentRepository childCommentRepository;

    @Autowired
    private ParentCommentService parentCommentService;


    @Override
    public ChildComment createChildComment(ChildCommentDtoBase childComment) {
        int idParentComment = Integer.parseInt(childComment.getIdParentComment());
        ParentComment parentComment = parentCommentService.getParentCommentByIdParentComment(idParentComment);


        String komen = childComment.getKomen();
        ChildComment cc = new ChildComment(komen, parentComment);
        childCommentRepository.save(cc);

        return cc;


    }

    @Override
    public Iterable<ChildComment> getListChildComment() {
        return childCommentRepository.findAll();
    }

    @Override
    public List<ChildComment> getChildCommentByParentComment(ParentComment parentComment) {
        return childCommentRepository.findByParentComment(parentComment);
    }

}
