package kelompokb4.moviedb.comment.service;

import kelompokb4.moviedb.comment.core.ParentComment;
import kelompokb4.moviedb.comment.repository.ParentCommentRepository;
import kelompokb4.moviedb.movie.core.Movie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ParentCommentServiceImpl implements ParentCommentService{
    @Autowired
    private ParentCommentRepository parentCommentRepository;


    @Override
    public ParentComment createParentComment(ParentComment parentComment) {
        parentCommentRepository.save(parentComment);
        return parentComment;
    }

    @Override
    public ParentComment getParentCommentByIdParentComment(int idParentComment) {
        return parentCommentRepository.findByIdParentComment(idParentComment);
    }

    @Override
    public Iterable<ParentComment> getListParentComment() {
        return parentCommentRepository.findAll();
    }

    @Override
    public List<ParentComment> getParentCommentByMovie(Movie movie) {
        int idMovie = movie.getIdMovie();
        return parentCommentRepository.findByIdMovie(idMovie);
    }
}
