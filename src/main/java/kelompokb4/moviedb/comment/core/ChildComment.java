package kelompokb4.moviedb.comment.core;

import kelompokb4.moviedb.comment.core.ParentComment;

import lombok.Data;
import lombok.NoArgsConstructor;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "child_comment")
@Data
@NoArgsConstructor
public class ChildComment {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int idChildComment;

    @Column(name = "username")
    private String username;

    @Column(name = "komen")
    private String komen;

    @Column(name = "tangga_post")
    private LocalDateTime tanggalPost;



    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "id_parent_comment")
    private ParentComment parentComment;

    public ChildComment(String komen, ParentComment parentComment) {
        this.username = "komeng";
        this.tanggalPost = LocalDateTime.now();
        this.komen = komen;
        this.parentComment = parentComment;
    }
}

