package kelompokb4.moviedb.comment.core;

import kelompokb4.moviedb.comment.core.ChildComment;
import kelompokb4.moviedb.movie.core.Movie;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.*;
import javax.persistence.*;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;


@Entity
@Table(name = "parent_comment")
@Data
@NoArgsConstructor
public class ParentComment {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int idParentComment;

    @Column(name = "username")
    private String username;

    @Column(name = "komen")
    private String komen;

    @Column(name = "tangga_post")
    private LocalDateTime tanggalPost;


    @OneToMany(mappedBy = "parentComment")
    @JsonManagedReference
    private List<ChildComment> childComments;

    @Column(name = "id_movie")
    private int idMovie;

    public ParentComment(String komen,String username, int idMovie) {
        this.username = username;
        this.komen = komen;
        this.tanggalPost = LocalDateTime.now();
        this.idMovie = idMovie;
    }
}
