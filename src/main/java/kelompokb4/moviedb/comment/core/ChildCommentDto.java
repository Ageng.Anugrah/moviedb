package kelompokb4.moviedb.comment.core;

public interface ChildCommentDto {
    public String getKomen();

    public String getIdParentComment();

}
