package kelompokb4.moviedb.comment.core;

public class ChildCommentDtoBase implements ChildCommentDto{

    private String komen;

    private String idparentcomment;

    @Override
    public String getKomen() {
        return this.komen;
    }

    @Override
    public String getIdParentComment() {
        return this.idparentcomment;
    }

    public void setKomen(String komen){
        this.komen = komen;
    }

    public void setIdparentcomment(String idparentcomment){
        this.idparentcomment = idparentcomment;
    }
}
