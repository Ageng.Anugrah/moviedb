package kelompokb4.moviedb.comment.controller;

import kelompokb4.moviedb.comment.service.ParentCommentService;
import kelompokb4.moviedb.comment.core.ChildComment;
import kelompokb4.moviedb.comment.service.ChildCommentService;
import kelompokb4.moviedb.comment.core.ParentComment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import kelompokb4.moviedb.comment.core.ChildCommentDtoBase;

import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;

@Controller
public class ChildCommentController {
    @Autowired
    private ChildCommentService childCommentService;

    @Autowired
    private ParentCommentService parentCommentService;

    @GetMapping("/{idParentComment}/childComments")
    public String getChildComments(@PathVariable("idParentComment") int idParentComment, Model model) {
        model.addAttribute("childComments", childCommentService.getListChildComment());
        ParentComment parentComment = parentCommentService.getParentCommentByIdParentComment(idParentComment);
        model.addAttribute("parentComment", parentComment);
        model.addAttribute("idParentComment", idParentComment);


        return "comment/comment";
    }

    @PostMapping(value = "/createchildComment", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity createChildComment(@RequestBody ChildCommentDtoBase childComment, HttpServletRequest request) {
            return ResponseEntity.ok(childCommentService.createChildComment(childComment));



    }
}
