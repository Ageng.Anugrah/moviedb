package kelompokb4.moviedb.comment.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import kelompokb4.moviedb.auth.service.MyUserDetailService;
import kelompokb4.moviedb.auth.service.UserService;
import kelompokb4.moviedb.comment.core.ParentComment;
import kelompokb4.moviedb.movie.core.Movie;
import kelompokb4.moviedb.movie.service.MovieService;
import kelompokb4.moviedb.comment.core.ChildCommentDtoBase;
import kelompokb4.moviedb.comment.service.ChildCommentService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;



import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


public class ChildCommentControllerTest {
    @Autowired
    private MockMvc mockMvc;


    private ChildCommentService childCommentService;


    private MovieService movieService;


    private MyUserDetailService myUserDetailService;


    private UserService userService;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private Movie movie;
    private ChildCommentDtoBase cc;

    @BeforeEach
    public void setUp(){
        //movie = new Movie("title", "1998", "desc");
        //ParentComment parentComment = new ParentComment("parent comment nih",movie);
        //mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        //cc = new ChildCommentDtoBase();
        //cc.setIdparentcomment(String.valueOf(parentComment.getIdParentComment()));
        //cc.setKomen("Komen");

    }



    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }





}
