package kelompokb4.moviedb.comment.controller;

import kelompokb4.moviedb.comment.service.ChildCommentService;
import kelompokb4.moviedb.movie.service.MovieService;
import kelompokb4.moviedb.comment.core.ParentComment;
import kelompokb4.moviedb.comment.service.ParentCommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;


import javax.servlet.http.HttpServletRequest;

@Controller
public class ParentCommentController {
    @Autowired
    private ParentCommentService parentCommentService;

    @Autowired
    private MovieService movieService;

    @Autowired
    private ChildCommentService childCommentService;

    @GetMapping("/parentComments")
    public String getParentComments(Model model) {
        model.addAttribute("parentComments", parentCommentService.getListParentComment());
        return "parentComment/parentComment";
    }

    @PostMapping("/createparentComment")
    public String createParentComment(HttpServletRequest request) {
        String komen = request.getParameter("komen");
        var idMovie = Integer.parseInt(request.getParameter("idmovie"));
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        var parentComment = new ParentComment(komen,username,idMovie);
        parentCommentService.createParentComment(parentComment);

        return "redirect:/movie/" + idMovie;
    }
    
}
