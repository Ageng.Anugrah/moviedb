package kelompokb4.moviedb.comment.repository;

import kelompokb4.moviedb.movie.core.Movie;
import kelompokb4.moviedb.comment.core.ParentComment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ParentCommentRepository extends JpaRepository<ParentComment, Integer> {
    ParentComment findByIdParentComment(int idParentComment);
    List<ParentComment> findByIdMovie(int idMovie);
}
