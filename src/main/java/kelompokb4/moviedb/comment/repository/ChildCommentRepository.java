package kelompokb4.moviedb.comment.repository;

import kelompokb4.moviedb.comment.core.ParentComment;
import kelompokb4.moviedb.comment.core.ChildComment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ChildCommentRepository extends JpaRepository<ChildComment, Integer> {
    ChildComment findByIdChildComment(int idChildComment);
    List<ChildComment> findByParentComment(ParentComment parentComment);
}
