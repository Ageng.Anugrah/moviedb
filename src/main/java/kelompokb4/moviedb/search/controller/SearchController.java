package kelompokb4.moviedb.search.controller;

import kelompokb4.moviedb.movie.core.Movie;
import kelompokb4.moviedb.movie.service.MovieService;
import kelompokb4.moviedb.search.repository.StrategyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import io.micrometer.core.annotation.Timed;

import java.util.*;

@Controller
public class SearchController {
    @Autowired
    private MovieService movieService;

    @Autowired
    private StrategyRepository strategyRepository;

    @Timed("sortingResult")
    @RequestMapping(method = RequestMethod.GET, value = "/result")
    public String sortingResult(@RequestParam("keyword") String keyword, @RequestParam("filter") String filter, Model model) {
        List<Movie> temp = new ArrayList<Movie>();
        //untuk microservice, sudah dihandle oleh movie, sehingga search tinggal menggunakannya saja
        for(Movie m: movieService.getListMovie()) {
            if(m.getTitle().toLowerCase().contains(keyword.toLowerCase())) {
                temp.add(m);
            }
        }
        Collections.sort(temp, strategyRepository.getSearchByType(filter));
        model.addAttribute("movies", temp);
        return "home";
    }

}
