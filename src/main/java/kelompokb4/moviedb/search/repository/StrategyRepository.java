package kelompokb4.moviedb.search.repository;

import kelompokb4.moviedb.movie.core.Movie;
import kelompokb4.moviedb.search.core.SearchByRating;
import kelompokb4.moviedb.search.core.SearchByTitle;
import kelompokb4.moviedb.search.core.SearchByYear;
import org.springframework.stereotype.Repository;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

//untuk microservice, sudah dihandle oleh movie, sehingga search tinggal menggunakannya saja

@Repository
public class StrategyRepository  {
    private Map<String, Comparator<Movie>> searchBy = new HashMap<>();

    public Comparator<Movie> getSearchByType(String alias) {
        return searchBy.get(alias);
    }

    public StrategyRepository() {
        searchBy.put("Title", new SearchByTitle());
        searchBy.put("Year", new SearchByYear());
        searchBy.put("Rating", new SearchByRating());
    }
}
