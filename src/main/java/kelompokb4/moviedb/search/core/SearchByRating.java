package kelompokb4.moviedb.search.core;

import kelompokb4.moviedb.movie.core.Movie;

import java.util.Comparator;

//untuk microservice, sudah dihandle oleh movie, sehingga search tinggal menggunakannya saja

public class SearchByRating implements Comparator<Movie> {
    public int compare(Movie a, Movie b)
    {
//        return b.getRating().compareTo(a.getRating());
        return Double.compare(b.getRating(), a.getRating());
    }
}
