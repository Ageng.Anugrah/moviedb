package kelompokb4.moviedb.search.core;

import kelompokb4.moviedb.movie.core.Movie;

import java.util.Comparator;

//untuk microservice, sudah dihandle oleh movie, sehingga search tinggal menggunakannya saja

public class SearchByYear implements Comparator<Movie> {
    public int compare(Movie a, Movie b)
    {
        return b.getYear().compareTo(a.getYear());
    }
}
