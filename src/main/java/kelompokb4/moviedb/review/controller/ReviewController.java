package kelompokb4.moviedb.review.controller;

import io.micrometer.core.annotation.Timed;
import kelompokb4.moviedb.movie.core.Movie;
import kelompokb4.moviedb.movie.service.MovieService;
import kelompokb4.moviedb.review.core.Review;
import kelompokb4.moviedb.review.core.ReviewDtoBase;
import kelompokb4.moviedb.review.service.ReviewService;
import kelompokb4.moviedb.review.service.error.AlreadyReviewException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@Controller
public class ReviewController {
    @Autowired
    private ReviewService reviewService;

    @Autowired
    private MovieService movieService;

    @GetMapping("/reviews")
    public String getReviews(Model model) {
        model.addAttribute("reviews", reviewService.getListReview());
        return "review/review";
    }

    @Timed("createreview")
    @PostMapping(value = "/createreview", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity createReview(@RequestBody ReviewDtoBase review, HttpServletRequest request) {
        try {
            return ResponseEntity.ok(reviewService.createReview(review));
        } catch (AlreadyReviewException ex) {
            return new ResponseEntity(new AlreadyReviewException("Anda sudah pernah menambahkan review untuk film ini"), HttpStatus.BAD_REQUEST);
        }
    }

}
