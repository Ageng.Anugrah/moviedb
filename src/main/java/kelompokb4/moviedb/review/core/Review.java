package kelompokb4.moviedb.review.core;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import kelompokb4.moviedb.movie.core.Movie;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

@Entity
@Table(name = "review")
@Data
@NoArgsConstructor
public class Review {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int idReview;

    @Column(name = "username")
    private String username;

    @Column(name = "nilai")
    private double nilai;

    @Column(name = "komen")
    private String komen;

    @Column(name = "tanggal_post")
    private String tanggalPost;

    @Column(name = "id_movie")
    private int idMovie;

    private String getFormatedDate () {
        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
        ZonedDateTime nowInJakarta = ZonedDateTime.now(ZoneId.of("Asia/Jakarta"));
        String formatedDate = nowInJakarta.format(format);
        return formatedDate;
    }

    public Review (double nilai, String komen, int idMovie, String username){
        this.nilai = nilai;
        this.komen = komen;
        this.idMovie = idMovie;
        this.username = username;
        this.tanggalPost = getFormatedDate();
    }

}
