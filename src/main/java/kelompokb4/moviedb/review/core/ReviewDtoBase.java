package kelompokb4.moviedb.review.core;

public class ReviewDtoBase implements ReviewDto{
    private String nilai;

    private String komen;

    private String idmovie;

    @Override
    public String getNilai() {
        return this.nilai;
    }

    @Override
    public String getKomen() {
        return this.komen;
    }

    @Override
    public String getIdmovie() {
        return this.idmovie;
    }

    public void setNilai(String nilai) {
        this.nilai=nilai;
    }

    public void setKomen(String komen) {
        this.komen=komen;
    }

    public void setIdmovie(String idmovie) {
        this.idmovie = idmovie;
    }
}
