package kelompokb4.moviedb.review.core;

public interface ReviewDto {
    public String getNilai();

    public String getKomen();

    public String getIdmovie();

}
