package kelompokb4.moviedb.review.repository;


import kelompokb4.moviedb.movie.core.Movie;
import kelompokb4.moviedb.review.core.Review;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReviewRepository extends JpaRepository<Review, Integer>{
    Review findByIdReview(int idReview);
    List<Review> findByIdMovie(int idMovie);
}
