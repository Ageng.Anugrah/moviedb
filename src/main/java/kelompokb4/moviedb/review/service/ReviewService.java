package kelompokb4.moviedb.review.service;

import kelompokb4.moviedb.movie.core.Movie;
import kelompokb4.moviedb.review.core.Review;
import kelompokb4.moviedb.review.core.ReviewDtoBase;
import kelompokb4.moviedb.review.service.error.AlreadyReviewException;

import java.util.List;

public interface ReviewService {
    Review createReview(ReviewDtoBase review) throws AlreadyReviewException;
    Iterable<Review> getListReview();
    List<Review> getReviewByMovie(Movie movie);
}
