package kelompokb4.moviedb.review.service;

import kelompokb4.moviedb.movie.core.Movie;
import kelompokb4.moviedb.movie.service.MovieService;
import kelompokb4.moviedb.review.core.Review;
import kelompokb4.moviedb.review.core.ReviewDtoBase;
import kelompokb4.moviedb.review.repository.ReviewRepository;
import kelompokb4.moviedb.review.service.error.AlreadyReviewException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ReviewServiceImpl implements ReviewService{
    @Autowired
    private ReviewRepository reviewRepository;

    @Autowired
    private MovieService movieService;

    @Override
    public Review createReview(ReviewDtoBase review) throws AlreadyReviewException {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        int idMovie = Integer.parseInt(review.getIdmovie());
        Movie movie = movieService.getMovieByIdMovie(idMovie);
        if (alreadyReview(username, movie)){
            throw new AlreadyReviewException("Anda sudah pernah menambahkan review untuk film ini");
        }
        double nilai = Double.parseDouble(review.getNilai());
        String komen = review.getKomen();
        Review rev = new Review(nilai, komen, idMovie, username);
        reviewRepository.save(rev);
        movieService.updateRating(movie);
        return rev;
}

    @Override
    public Iterable<Review> getListReview() {
        return reviewRepository.findAll();
    }

    @Override
    public List<Review> getReviewByMovie(Movie movie) {
        int idMovie = movie.getIdMovie();
        return reviewRepository.findByIdMovie(idMovie);
    }

    public Boolean alreadyReview(String username, Movie movie) {
        List<Review> movieReview = reviewRepository.findByIdMovie(movie.getIdMovie());
        for (Review r : movieReview){
            if (r.getUsername().equalsIgnoreCase(username))
                return true;
        }
        return false;
    }
}
