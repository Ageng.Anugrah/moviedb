package kelompokb4.moviedb.review.service.error;

public class AlreadyReviewException extends Exception{

    public AlreadyReviewException(String message){
        super(message);
    }
}
