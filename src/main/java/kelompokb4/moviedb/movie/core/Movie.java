package kelompokb4.moviedb.movie.core;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import kelompokb4.moviedb.comment.core.ParentComment;
import com.fasterxml.jackson.annotation.JsonBackReference;
import kelompokb4.moviedb.review.core.Review;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Movie {
    int idMovie;

    private String title;

    private String year;

    private String description;

    private String url;

    private double rating;

    public String getTitle() {
        return title;
    }

    public String getYear() {
        return year;
    }

    public double getRating() { return rating;}

}
