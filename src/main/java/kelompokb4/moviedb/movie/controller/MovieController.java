package kelompokb4.moviedb.movie.controller;

import kelompokb4.moviedb.comment.core.ChildComment;
import kelompokb4.moviedb.comment.core.ParentComment;
import kelompokb4.moviedb.comment.service.ChildCommentService;
import kelompokb4.moviedb.comment.service.ParentCommentService;
import io.micrometer.core.annotation.Timed;
import kelompokb4.moviedb.movie.core.Movie;
import kelompokb4.moviedb.movie.service.MovieService;
import kelompokb4.moviedb.review.service.ReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
public class MovieController {
    @Autowired
    private MovieService movieService;

    @Autowired
    private ReviewService reviewService;

    @Autowired
    private ParentCommentService parentCommentService;

    @Autowired
    private ChildCommentService childCommentService;

    @Timed("homee")
    @GetMapping("/")
    public String home(Model model) {
        model.addAttribute("movies", movieService.getListMovie());
        return "home";
    }

    @Timed("getmovieandreview")
    @GetMapping("/movie/{idMovie}")
    public String getMovieById(@PathVariable("idMovie") int idMovie, Model model) {

        var movie = movieService.getMovieByIdMovie(idMovie);
        var childComments = generateChildComments(movie);
        model.addAttribute("movie", movie);
        model.addAttribute("reviews", reviewService.getReviewByMovie(movie));
        model.addAttribute("parentComments",parentCommentService.getParentCommentByMovie(movie));
        model.addAttribute("childComments", childComments);
        

        return "movie/movie";
    }

    private List<ChildComment> generateChildComments(Movie movie){
        List<ChildComment> childComments = new ArrayList<ChildComment>();
        if (parentCommentIsNotEmpty(movie) == true)
            for (ParentComment i: parentCommentService.getParentCommentByMovie(movie)) {
                if(childCommentService.getChildCommentByParentComment(i) != null)
                    for (ChildComment j : childCommentService.getChildCommentByParentComment(i)) {
                        childComments.add(j);
                    }
            }
        return childComments;
    }

    private boolean parentCommentIsNotEmpty(Movie movie){
        if(parentCommentService.getParentCommentByMovie(movie) != null) return true;
        else return false;
    }



}
