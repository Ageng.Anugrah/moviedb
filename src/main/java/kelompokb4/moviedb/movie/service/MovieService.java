package kelompokb4.moviedb.movie.service;

import kelompokb4.moviedb.movie.core.Movie;

import java.util.List;

public interface MovieService {
    //List atau iterable
    Movie getMovieByIdMovie(int idMovie);
    List<Movie> getListMovie();
    void updateRating(Movie movie);

}
