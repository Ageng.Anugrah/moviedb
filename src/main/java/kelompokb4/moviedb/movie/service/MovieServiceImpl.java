package kelompokb4.moviedb.movie.service;

import kelompokb4.moviedb.movie.core.Movie;
import kelompokb4.moviedb.review.core.Review;
import kelompokb4.moviedb.review.repository.ReviewRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import java.util.List;

@Service
public class MovieServiceImpl implements MovieService{

    @Autowired
    private ReviewRepository reviewRepository;


    @Override
    public Movie getMovieByIdMovie(int idMovie) {
        RestTemplate restTemplate = new RestTemplate();
        Movie movie = restTemplate.getForObject("https://moviedb-movie.herokuapp.com/movie/" + idMovie, Movie.class);
        return movie;
    }

    @Override
    public List<Movie> getListMovie() {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<List<Movie>> responseEntity = restTemplate.exchange(
                "https://moviedb-movie.herokuapp.com/movie/",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<Movie>>() {}
        );
        List<Movie> movies = responseEntity.getBody();
        return movies;
    }

    @Override
    public void updateRating(Movie movie) {
        RestTemplate restTemplate = new RestTemplate();
        double jumlah = 0;
        List<Review> reviews = reviewRepository.findByIdMovie(movie.getIdMovie());
        for (Review review : reviews) {
            jumlah += review.getNilai();
        }
        double avg = Math.floor(jumlah / reviews.size() * 100) / 100;
        movie.setRating(avg);
        String url = "https://moviedb-movie.herokuapp.com/movie/rating/" + movie.getIdMovie() + "?rating=" + avg;
        restTemplate.put(url, movie);
    }
}
