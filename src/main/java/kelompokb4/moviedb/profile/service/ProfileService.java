package kelompokb4.moviedb.profile.service;


import kelompokb4.moviedb.auth.model.User;

public interface ProfileService {

    public User findUserByUsername(String username);
}
