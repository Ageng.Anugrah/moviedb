package kelompokb4.moviedb.profile.service;

import kelompokb4.moviedb.auth.model.User;
import kelompokb4.moviedb.auth.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProfileServiceImpl implements ProfileService{

    @Autowired
    private UserRepository userRepository;

    public User findUserByUsername(String username){ return userRepository.findByUsername(username);}

}
