package kelompokb4.moviedb.profile.controller;

import kelompokb4.moviedb.profile.service.ProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class ProfileController {
    @Autowired
    ProfileService profileService;

    @GetMapping("/profile/{username}")
    public String getUserByUsername(@PathVariable("username") String username, Model model){
        var user = profileService.findUserByUsername(username);
        model.addAttribute("user", user);
        return "profile/profile";
    }
}
