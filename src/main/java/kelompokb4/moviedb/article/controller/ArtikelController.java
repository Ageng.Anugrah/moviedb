package kelompokb4.moviedb.article.controller;

import io.micrometer.core.annotation.Timed;
import kelompokb4.moviedb.article.core.artikel.Artikel;
import kelompokb4.moviedb.article.service.ArtikelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

@Controller
public class ArtikelController {

    @Autowired
    private ArtikelService artikelService;

    @Timed("getartikel")
    @GetMapping("/artikels")
    public String getArtikels(Model model) {
        model.addAttribute("artikels", artikelService.getListArtikel());
        return "article/artikel";
    }

    @Timed("halamancreateartikel")
    @GetMapping("/createartikel")
    public String createArtikel(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();
        if (username.equalsIgnoreCase("anonymousUser")) {
            return "redirect:/";
        } else {
            return "article/createartikel";
        }
    }

    @Timed("postartikel")
    @PostMapping("/createartikel")
    public String createArtikel(HttpServletRequest request) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();
        if (username.equalsIgnoreCase("anonymousUser")) {
            return "redirect:/";
        } else {
            String judul = request.getParameter("judul");
            String isi = request.getParameter("isi");
            Artikel artikel = new Artikel(judul, isi, username);
            artikelService.createArtikel(artikel);
            return "redirect:/artikels";
        }
    }

    @Timed("getartikelbyid")
    @Transactional
    @GetMapping("/artikel/{idArtikel}")
    public String getMovieById(@PathVariable("idArtikel") int idArtikel, Model model) {
        Artikel artikel = artikelService.getArtikelById(idArtikel);
        model.addAttribute("artikel", artikel);
        return "article/showartikel";
    }

}