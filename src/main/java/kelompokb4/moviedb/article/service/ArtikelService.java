package kelompokb4.moviedb.article.service;

import kelompokb4.moviedb.article.core.artikel.Artikel;
import kelompokb4.moviedb.movie.core.Movie;

public interface ArtikelService {
    void createArtikel(Artikel artikel);
    Iterable<Artikel> getListArtikel();
    Artikel getArtikelById(int idArtikel);
}
