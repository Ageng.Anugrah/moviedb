package kelompokb4.moviedb.article.service;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import kelompokb4.moviedb.article.core.artikel.Artikel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import java.io.IOException;
import java.util.List;

@Service
public class ArtikelServiceImpl implements ArtikelService {

    @Override
    public void createArtikel(Artikel artikel) {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        String url = "https://moviedb-article.herokuapp.com/article/";
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String json = "";
        try {
            json = ow.writeValueAsString(artikel);
        } catch (Exception e) {
            json = "";
        }
        HttpEntity<String> entity = new HttpEntity<String>(json,headers);
        String answer = restTemplate.postForObject(url, entity, String.class);
    }

    @Override
    public Iterable<Artikel> getListArtikel() {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<List<Artikel>> responseEntity = restTemplate.exchange(
                "https://moviedb-article.herokuapp.com/article/",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<Artikel>>() {}
        );
        List<Artikel> artikels = responseEntity.getBody();
        return artikels;
    }

    @Override
    public Artikel getArtikelById(int idArtikel) {
        RestTemplate restTemplate = new RestTemplate();
        Artikel artikel = restTemplate.getForObject("https://moviedb-article.herokuapp.com/article/" + idArtikel, Artikel.class);
        return artikel;
    }

}
