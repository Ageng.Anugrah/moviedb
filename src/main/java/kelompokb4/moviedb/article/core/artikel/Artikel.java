package kelompokb4.moviedb.article.core.artikel;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

@Data
@NoArgsConstructor
public class Artikel {
    private int idArtikel;

    private String username;

    private String judul;

    private String isi;

    private String tanggalPost;

    private String getFormatedDate () {
        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
        ZonedDateTime nowInJakarta = ZonedDateTime.now(ZoneId.of("Asia/Jakarta"));
        String formatedDate = nowInJakarta.format(format);
        return formatedDate;
    }

    public Artikel (String judul, String isi, String username){
        this.judul = judul;
        this.isi = isi;
        this.username = username;
        this.tanggalPost = getFormatedDate();
    }
}
