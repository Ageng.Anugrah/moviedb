#### Master Branch Coverage / Pipeline
| Pipeline | Coverage |Quality Gate Status|
|---       |---       |---                |
|[![pipeline status](https://gitlab.com/Ageng.Anugrah/moviedb/badges/master/pipeline.svg)](https://gitlab.com/Ageng.Anugrah/moviedb/-/commits/master)|[![coverage report](https://gitlab.com/Ageng.Anugrah/moviedb/badges/master/coverage.svg)](https://gitlab.com/Ageng.Anugrah/moviedb/-/commits/master)|[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?branch=master&project=Ageng.Anugrah_moviedb&metric=alert_status)](https://sonarcloud.io/dashboard?id=Ageng.Anugrah_moviedb&branch=master)|

## Anggota Kelompok TK Advance Programming B4
|No | Nama                      | NPM       |
|---|---                        |---        |
|1. |Ageng Anugrah Wardoyo Putra| 1906398212|
|2. |Carissa Syieva Qalbiena    | 1806186875|
|3. |Ferdi Fadillah             | 1906351083|
|4. |Jovanta Anugerah Pelawi    | 1606875863|
|5. |Rheznandya Erwanto         | 1906318924|

## Project Description
Ini adalah aplikasi berbasis web yang memiliki fungsi sebagai platform untuk penggemar film yang ingin membuat review, artikel, berkomentar dan mencari rekomendasi film. 

## Profiling pada fitur review dan article
![Profiling](images/profiling.png) 

Dapat dilihat melalui grafik diatas bahwa fitur untuk getmovieandreview serta getartikel memiliki load time yang cenderung lebih lama dari fitur lainnya.
Hal ini disebabkan karena pada fitur getmovieandreview harus melakukan query terlebih dahulu untuk mencari film yang sesuai dengan id yang diminta kemudian mencari review yang sesuai dengan film tersebut.
Hal yang sama terjadi pada fitur getartikel dimana setiap pada fitur tersebut harus menjalankan query untuk mencari semua artikel yang dibuat oleh user baru ditampilkan pada halaman.

Untuk refactoring pada fitur-fitur yang telah disebutkan diatas saya rasa belum dibutuhkan karena memang load timenya sejalan dengan jumlah query yang dijalankan kompleksitas hampir mendekati linear (O(n)).
Oleh karena itu untuk mempercepat query ini secara optimal bukanlah memodifikasi fungsi pada program java melainkan melakukan sorting database yang ada di postgres, hal ini dapat dicapai dengan melakukan indexing pada postgres.